package com.smartbear.spokentime.converter.british;

import com.smartbear.spokentime.converter.input.TimeToken;

public class SpokenPhraseStrategyResolverImpl implements SpokenPhraseStrategyResolver {
  @Override
  public SpokenPhraseStrategy resolve(TimeToken timeToken) {
    if (timeToken.isMidnight()) {
      return new ConstantStrategyImpl(Constants.MIDNIGHT);
    }

    if (timeToken.isNoon()) {
      return new ConstantStrategyImpl(Constants.NOON);
    }

    if (timeToken.isFullHour()) {
      return new FullHourStrategyImpl();
    }

    if (timeToken.isHalfPast()) {
      return new HalfPastStrategyImpl();
    }

    if (timeToken.isQuarter()) {
      return new QuarterStrategyImpl();
    }

    return new PastOrToStrategyImpl();
  }
}
