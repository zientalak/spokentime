package com.smartbear.spokentime.converter.british;

public enum PmAmMode {
  AM("in the morning"),
  PM("in the evening"),
  DISABLED("");

  private final String mode;

  PmAmMode(String mode) {
    this.mode = mode;
  }

  @Override
  public String toString() {
    return mode;
  }
}
