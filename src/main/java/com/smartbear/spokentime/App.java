package com.smartbear.spokentime;

import com.smartbear.spokentime.converter.SpokenTime;
import com.smartbear.spokentime.converter.input.BritishSpokenTimeFactory;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import picocli.CommandLine;

@CommandLine.Command(
    name = "spokentime",
    description = "Transform time to spoken time",
    version = "1.0",
    mixinStandardHelpOptions = true)
public class App implements Callable<Integer> {

  @CommandLine.Option(
      names = {"-l", "--language"},
      description = "Target language",
      defaultValue = "british")
  String language;

  @CommandLine.Option(
      names = {"-i", "--input"},
      description = "Comma separated time e.q. 12:05,23:33",
      required = true,
      split = ",")
  Set<String> input;

  public static void main(String[] args) {
    int exitCode = new CommandLine(new App()).execute(args);
    System.exit(exitCode);
  }

  @Override
  public Integer call() throws Exception {
    // could be DI in web app
    Map<String, SpokenTime> services = new HashMap<>();
    services.put("british", BritishSpokenTimeFactory.create());
    if (!services.containsKey(language)) {
      System.out.println(String.format("Language \"%s\" is not supported.", language));
      return 1;
    }

    SpokenTime spokenTimeService = services.get(language);

    try {
      input.stream()
          .map(String::trim)
          .forEach(
              time -> {
                System.out.printf("%s - %s%n", time, spokenTimeService.convert(time));
              });
    } catch (Exception e) {
      System.out.println(e.getMessage());
      return 1;
    }

    return 0;
  }
}
