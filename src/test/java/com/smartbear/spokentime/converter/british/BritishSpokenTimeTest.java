package com.smartbear.spokentime.converter.british;

import static org.assertj.core.api.Assertions.assertThat;

import com.smartbear.spokentime.converter.SpokenTime;
import com.smartbear.spokentime.converter.input.BritishSpokenTimeFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

class BritishSpokenTimeTest {
  private SpokenTime spokenTime;

  @BeforeEach
  void setUp() {
    spokenTime = BritishSpokenTimeFactory.create();
  }

  @ParameterizedTest
  @CsvFileSource(
      resources = {
        "/full-hours.csv",
        "/full-hours-am-pm.csv",
        "/full-hours-24h.csv",
        "/half-past.csv",
        "/past-two.csv",
        "/quarter.csv"
      },
      numLinesToSkip = 1)
  public void itConvertTimeToEnglishSpoken(String input, String output) {
    assertThat(spokenTime.convert(input))
        .as("Time input %s should be converted to %s", input, output)
        .isEqualTo(output);
  }
}
