package com.smartbear.spokentime.converter.input;

public interface InputParser {
  TimeToken parse(String input);
}
