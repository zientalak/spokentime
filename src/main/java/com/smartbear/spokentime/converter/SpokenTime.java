package com.smartbear.spokentime.converter;

public interface SpokenTime {
  String convert(String input);
}
