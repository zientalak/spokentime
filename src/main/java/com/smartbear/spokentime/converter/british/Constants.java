package com.smartbear.spokentime.converter.british;

public final class Constants {
  public static final String MINUTE = "minute";
  public static final String HALF_PAST = "half past";
  public static final String MIDNIGHT = "midnight";
  public static final String NOON = "noon";
  public static final String A_CLOCK = "%s o'clock";
  public static final String A_QUARTER = "a quarter";
  public static final String PAST = "past";
  public static final String TO = "to";
  public static final String MINUTES = "minutes";
}
