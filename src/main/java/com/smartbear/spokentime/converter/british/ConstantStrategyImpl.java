package com.smartbear.spokentime.converter.british;

public class ConstantStrategyImpl implements SpokenPhraseStrategy {
  private final String phrase;

  public ConstantStrategyImpl(String phrase) {
    this.phrase = phrase;
  }

  @Override
  public String buildPhrase(PmAmMode pmAmMode, Labels labels) {
    return phrase;
  }
}
