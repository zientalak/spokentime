package com.smartbear.spokentime.converter.input;

import java.time.LocalTime;

public class TimeToken {
  private final String input;
  private final LocalTime time;

  public TimeToken(String input, LocalTime time) {
    this.input = input;
    this.time = time;
  }

  public boolean isQuarter() {
    return time.getMinute() == 15 || time.getMinute() == 45;
  }

  public boolean isFullHour() {
    return time.getMinute() == 0;
  }

  public boolean areMinutesDividedBy(int num) {
    return time.getMinute() % num == 0;
  }

  public boolean isPastToHour() {
    return time.getMinute() <= 30;
  }

  public boolean isHalfPast() {
    return time.getMinute() == 30;
  }

  public boolean isOneMinute() {
    return time.getMinute() == 1 || time.getMinute() == 59;
  }

  public boolean isMidnight() {
    return time.getHour() == 0 && time.getMinute() == 0;
  }

  public boolean isNoon() {
    return time.getHour() == 12 && time.getMinute() == 0;
  }

  public int getHour() {
    return time.getHour();
  }

  public int getMinute() {
    return time.getMinute();
  }

  public boolean is24HourFormat() {
    return this.time.getHour() > 12;
  }

  public boolean isAm() {
    return input.toLowerCase().contains("am");
  }

  public boolean isPm() {
    return input.toLowerCase().contains("pm");
  }
}
