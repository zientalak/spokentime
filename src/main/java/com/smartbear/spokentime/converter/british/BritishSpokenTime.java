package com.smartbear.spokentime.converter.british;

import com.smartbear.spokentime.converter.SpokenTime;
import com.smartbear.spokentime.converter.input.InputParser;
import com.smartbear.spokentime.converter.input.TimeToken;
import com.smartbear.spokentime.converter.utils.NumberToWords;

public class BritishSpokenTime implements SpokenTime {
  private final InputParser parser;
  private final NumberToWords numberToWords;
  private final SpokenPhraseStrategyResolver resolver;

  public BritishSpokenTime(
      InputParser parser, NumberToWords numberToWords, SpokenPhraseStrategyResolver resolver) {
    this.parser = parser;
    this.numberToWords = numberToWords;
    this.resolver = resolver;
  }

  @Override
  public String convert(String input) {
    TimeToken timeToken = parser.parse(input);
    PmAmMode pmAmMode = resolvePmAmMode(timeToken);
    Labels labels = resolveLabels(timeToken, pmAmMode);

    SpokenPhraseStrategy strategy = resolver.resolve(timeToken);
    return strategy.buildPhrase(pmAmMode, labels);
  }

  private PmAmMode resolvePmAmMode(TimeToken timeToken) {
    if (timeToken.is24HourFormat()) {
      return PmAmMode.PM;
    }

    if (timeToken.isAm()) {
      return PmAmMode.AM;
    }

    if (timeToken.isPm()) {
      return PmAmMode.PM;
    }

    return PmAmMode.DISABLED;
  }

  private Labels resolveLabels(TimeToken timeToken, PmAmMode pmAmMode) {
    int hour = timeToken.getHour();
    if (timeToken.is24HourFormat() || hour == 12) {
      hour = hour - 12;
    }

    boolean isPastToHour = timeToken.isPastToHour();
    if (!isPastToHour) {
      hour = hour + 1;
    }

    int minute = timeToken.getMinute();
    if (!isPastToHour) {
      minute = 60 - minute;
    }

    String hourLabel = numberToWords.convert(hour);
    String minuteLabel = numberToWords.convert(minute);
    String pastOrToLabel = isPastToHour ? Constants.PAST : Constants.TO;
    String minutesAnnotationLabel = "";
    if (!timeToken.areMinutesDividedBy(5)) {
      minutesAnnotationLabel = timeToken.isOneMinute() ? Constants.MINUTE : Constants.MINUTES;
    }

    if (hour == 0 && PmAmMode.DISABLED.equals(pmAmMode)) {
      hourLabel = numberToWords.convert(12);
    }

    if (hour == 0 && !PmAmMode.DISABLED.equals(pmAmMode)) {
      hourLabel = numberToWords.convert(12);
    }

    return new Labels(hourLabel, minuteLabel, pastOrToLabel, minutesAnnotationLabel);
  }
}
