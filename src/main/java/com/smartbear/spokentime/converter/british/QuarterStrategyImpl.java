package com.smartbear.spokentime.converter.british;

public class QuarterStrategyImpl implements SpokenPhraseStrategy {
  @Override
  public String buildPhrase(PmAmMode pmAmMode, Labels labels) {
    StringBuilder builder = new StringBuilder();
    builder
        .append(Constants.A_QUARTER)
        .append(" ")
        .append(labels.pastOrToLabel)
        .append(" ")
        .append(labels.hourLabel);

    if (!PmAmMode.DISABLED.equals(pmAmMode)) {
      builder.append(" ").append(pmAmMode);
    }

    return builder.toString();
  }
}
