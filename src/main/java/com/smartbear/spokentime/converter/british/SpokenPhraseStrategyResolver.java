package com.smartbear.spokentime.converter.british;

import com.smartbear.spokentime.converter.input.TimeToken;

public interface SpokenPhraseStrategyResolver {
  SpokenPhraseStrategy resolve(TimeToken timeToken);
}
