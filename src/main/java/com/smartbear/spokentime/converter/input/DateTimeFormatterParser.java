package com.smartbear.spokentime.converter.input;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class DateTimeFormatterParser implements InputParser {
  private final DateTimeFormatter formatter;

  public DateTimeFormatterParser(DateTimeFormatter formatter) {
    this.formatter = formatter;
  }

  @Override
  public TimeToken parse(String input) {
    try {
      return new TimeToken(input, LocalTime.parse(input, formatter));
    } catch (Exception ex) {
      throw new IllegalArgumentException(
          String.format("Wrong input time format (%s)", formatter.toString()), ex);
    }
  }
}
