package com.smartbear.spokentime.converter.british;

public class FullHourStrategyImpl implements SpokenPhraseStrategy {
  @Override
  public String buildPhrase(PmAmMode pmAmMode, Labels labels) {
    if (PmAmMode.DISABLED.equals(pmAmMode)) {
      return String.format(Constants.A_CLOCK, labels.hourLabel);
    }

    return String.format("%s %s", labels.hourLabel, pmAmMode);
  }
}
