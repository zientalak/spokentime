package com.smartbear.spokentime.converter.input;

import com.smartbear.spokentime.converter.SpokenTime;
import com.smartbear.spokentime.converter.british.BritishNumberToWords;
import com.smartbear.spokentime.converter.british.BritishSpokenTime;
import com.smartbear.spokentime.converter.british.SpokenPhraseStrategyResolverImpl;
import java.time.format.DateTimeFormatter;

public class BritishSpokenTimeFactory {
  public static SpokenTime create() {
    DateTimeFormatter formatter =
        DateTimeFormatter.ofPattern(
            ""
                + "[h:mm a]"
                + "[K:mm a]"
                + "[hh:mm a]"
                + "[KK:mm a]"
                + "[HH:mm]"
                + "[hh:mm]"
                + "[KK:mm]"
                + "[kk:mm]"
                + "[k:mm]"
                + "[K:mm]");

    DateTimeFormatterParser dateTimeFormatterParser = new DateTimeFormatterParser(formatter);

    return new BritishSpokenTime(
        dateTimeFormatterParser,
        new BritishNumberToWords(),
        new SpokenPhraseStrategyResolverImpl());
  }
}
