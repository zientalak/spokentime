package com.smartbear.spokentime.converter.british;

public interface SpokenPhraseStrategy {
  String buildPhrase(PmAmMode pmAmMode, Labels labels);
}
