package com.smartbear.spokentime.converter.british;

public class PastOrToStrategyImpl implements SpokenPhraseStrategy {
  @Override
  public String buildPhrase(PmAmMode pmAmMode, Labels labels) {
    StringBuilder builder = new StringBuilder();
    builder.append(labels.minuteLabel);
    if (!labels.minutesAnnotationLabel.isEmpty()) {
      builder.append(" ").append(labels.minutesAnnotationLabel);
    }

    builder.append(" ").append(labels.pastOrToLabel).append(" ").append(labels.hourLabel);

    if (!PmAmMode.DISABLED.equals(pmAmMode)) {
      builder.append(" ").append(pmAmMode);
    }

    return builder.toString();
  }
}
