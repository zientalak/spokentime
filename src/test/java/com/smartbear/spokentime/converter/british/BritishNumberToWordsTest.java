package com.smartbear.spokentime.converter.british;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class BritishNumberToWordsTest {
  private BritishNumberToWords numberToWords;

  @BeforeEach
  public void setUp() {
    numberToWords = new BritishNumberToWords();
  }

  @ParameterizedTest
  @MethodSource("integerAndWordProvider")
  public void itShouldConvertIntegerToWord(int number, String word) {
    assertThat(numberToWords.convert(number)).isEqualTo(word);
  }

  static Stream<Arguments> integerAndWordProvider() {
    return Stream.of(
        arguments(0, "zero"),
        arguments(1, "one"),
        arguments(2, "two"),
        arguments(3, "three"),
        arguments(4, "four"),
        arguments(5, "five"),
        arguments(6, "six"),
        arguments(7, "seven"),
        arguments(8, "eight"),
        arguments(9, "nine"),
        arguments(10, "ten"),
        arguments(11, "eleven"),
        arguments(12, "twelve"),
        arguments(13, "thirteen"),
        arguments(14, "fourteen"),
        arguments(15, "fifteen"),
        arguments(16, "sixteen"),
        arguments(17, "seventeen"),
        arguments(18, "eighteen"),
        arguments(19, "nineteen"),
        arguments(20, "twenty"),
        arguments(33, "thirty three"),
        arguments(96, "ninety six"),
        arguments(120, "one hundred twenty"),
        arguments(257, "two hundred fifty seven"),
        arguments(2009, "two thousand nine"),
        arguments(4762, "four thousand seven hundred sixty two"),
        arguments(87628, "eighty seven thousand six hundred twenty eight"),
        arguments(276286, "two lakh seventy six thousand two hundred eighty six"),
        arguments(4276286, "forty two lakh seventy six thousand two hundred eighty six"),
        arguments(
            842762860,
            "eighty four crore twenty seven lakh sixty two thousand eight hundred sixty"));
  }
}
