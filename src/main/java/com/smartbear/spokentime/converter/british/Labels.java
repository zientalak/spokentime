package com.smartbear.spokentime.converter.british;

public class Labels {
  public final String hourLabel;
  public final String minuteLabel;
  public final String pastOrToLabel;
  public final String minutesAnnotationLabel;

  public Labels(
      String hourLabel, String minuteLabel, String pastOrToLabel, String minutesAnnotationLabel) {
    this.hourLabel = hourLabel;
    this.minuteLabel = minuteLabel;
    this.pastOrToLabel = pastOrToLabel;
    this.minutesAnnotationLabel = minutesAnnotationLabel;
  }
}
