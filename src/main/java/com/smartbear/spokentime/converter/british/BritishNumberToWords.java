package com.smartbear.spokentime.converter.british;

import com.smartbear.spokentime.converter.utils.NumberToWords;

public class BritishNumberToWords implements NumberToWords {
  private static final String[] units = {
    "",
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
    "ten",
    "eleven",
    "twelve",
    "thirteen",
    "fourteen",
    "fifteen",
    "sixteen",
    "seventeen",
    "eighteen",
    "nineteen"
  };

  private static final String[] tens = {
    "", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"
  };

  @Override
  public String convert(int number) {
    if (number == 0) {
      return "zero";
    }

    if (number < 0) {
      return "minus " + convert(-number);
    }

    if (number < 20) {
      return units[number];
    }

    if (number < 100) {
      return tens[number / 10] + ((number % 10 != 0) ? " " : "") + units[number % 10];
    }

    if (number < 1000) {
      return units[number / 100]
          + " hundred"
          + ((number % 100 != 0) ? " " : "")
          + convert(number % 100);
    }

    if (number < 100000) {
      return convert(number / 1000)
          + " thousand"
          + ((number % 10000 != 0) ? " " : "")
          + convert(number % 1000);
    }

    if (number < 10000000) {
      return convert(number / 100000)
          + " lakh"
          + ((number % 100000 != 0) ? " " : "")
          + convert(number % 100000);
    }

    return convert(number / 10000000)
        + " crore"
        + ((number % 10000000 != 0) ? " " : "")
        + convert(number % 10000000);
  }
}
