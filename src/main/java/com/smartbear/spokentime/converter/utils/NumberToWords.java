package com.smartbear.spokentime.converter.utils;

public interface NumberToWords {
  String convert(int number);
}
