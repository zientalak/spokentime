package com.smartbear.spokentime.converter.input.exception;

public class ParseTimeException extends RuntimeException {
  public ParseTimeException(String message, Throwable cause) {
    super(message, cause);
  }

  public ParseTimeException(String message) {
    super(message);
  }
}
