# SpokenTime

This is CLI application that convert time to spoken form.

It is possible to use below formats:

```
0:01
00:01
12:00
13:00
01:00 PM
01:00 AM
21:00
```

## Code Examples
To run program just call: 

`./gradlew run --args="--input=\"12:00,17:34,03:15 PM\""`

There is also `--language` option in CLI with default value set on `british`
